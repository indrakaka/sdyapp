package id.web.saungbudaya.sdyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

//import id.web.saungbudaya.sdyapp.JobMain;

/**
 * Created by USER on 3/27/2018.
 */

public class Login extends AppCompatActivity {
    TextView Masuk;
    EditText username,password;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    String temp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login2);

        //Masuk = (TextView)findViewById(R.id.Masuk1);

        /*Masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ceklogin();
            }
        });*/
        final Button btndivisi = findViewById(R.id.Masuk1);
        btndivisi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent it = new Intent(Login.this, MainActivityAnggota.class);
                startActivity(it);
            }
        });
    }


    public void ceklogin(){
        username = (EditText) findViewById(R.id.NIP);
        password = (EditText)findViewById(R.id.password);
        radioGroup = (RadioGroup) findViewById(R.id.jenis);
        int selectedId = radioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        String temp2= (String) radioButton.getText();


        if(temp2.equals("Pengurus")){
            temp="pengurus";

        }else if(temp2.equals("Anggota")){
            temp="anggota";
        }else{
            temp="";
        }

        final ProgressDialog mProgress;
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Login");
        mProgress.setMessage("Mohon Tunggu...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        mProgress.show();

        Ion.with(getApplicationContext())
                .load("http://blue-banana-bear.xyz/ind ex.php/auth/")
                .setBodyParameter("username", username.getText().toString())
                .setBodyParameter("password", password.getText().toString())
                .setBodyParameter("level", temp)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {
                            JSONObject json = new JSONObject(result);    // Converts the string "result" to a JSONObject
                            String json_result = json.getString("login"); // Get the string "result" inside the Json-object
                            if (json_result.equalsIgnoreCase("true")){ // Checks if the "result"-string is equals to "ok"
                                // Result is "OK"
                                String nta = json.getString("NTA"); // I don't need to explain this one, right
                                String level = json.getString("level");
                                mProgress.dismiss();



                                if("pengurus".equals(level)){
                                    SharedPreferences sharedpreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString("NTA", nta);
                                    editor.commit();

                                    Intent it = new Intent(Login.this,MainActivity.class);
                                    Bundle b = new Bundle();
                                    b.putString("nta", nta);
                                    it.putExtras(b);
                                    startActivity(it);
                                }else if("anggota".equals(level)){
                                   // Intent it = new Intent(Login.this,JobMain.class);

                                    //startActivity(it);

                                }

                            }else{
                                mProgress.dismiss();
                                alert("Silahkan Cek Username & Password!");
                            }
                        } catch (JSONException v){
                            alert("LOGIN GAGAL!");
                            mProgress.dismiss();
                        }

                    }
                });

    }



    public void alert(String tampil){
        AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(tampil);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }
}