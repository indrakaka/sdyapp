package id.web.saungbudaya.sdyapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by USER on 7/12/2018.
 */

public class TambahJOB extends AppCompatActivity {
    @Override



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tambah_job);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btnTambah);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Selamat,Job telah berhasil ditambahkan!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }


}
